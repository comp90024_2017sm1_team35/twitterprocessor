# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

from __future__ import division
import couchdb
from classifer import classifer
import find_in_hospital
import json
import math
import time


remoteServer_string = "http://130.56.249.50:5984"
localServer_string = "http://127.0.0.1:5984/"

remoteServer = couchdb.Server(remoteServer_string)
localServer = couchdb.Server(localServer_string)
twitter_db = remoteServer['twitter_storage']
analysis = localServer['analysis_result']
hospital_result = localServer['hospital_result']
clf = classifer("nb.pkl","vocabulary.pkl")

result = dict()
result_negative = dict()
final_result = dict()



def get_bed_cat_by_id(hospital_id):
    #print "current hospital_id is " + str(hospital_id)
    json_object = json.load(open("hospital.geojson.json"))
    for hospital_info in json_object['features']:
        if hospital_info['id'] == hospital_id:
            return hospital_info['properties']['bed_cat']
    return None

def analysis_and_count(coordinates, text):
    hospital_id = find_in_hospital.contains_point(coordinates)
    if not hospital_id:
        print "analysing text: "

    else:
        #print "analysing text: "+text
        motivation = clf.prediction(text)
        #get the bed_cat from the file by the hospital_id
        bed_cat = get_bed_cat_by_id(hospital_id)
        if not bed_cat:
            bed_cat = 0
        #print "bed_cat is" + str(bed_cat)
        if motivation == 1:
            # it is positive
            if result.get(bed_cat, None):
                result[bed_cat] += 1
            else:
                result[bed_cat] = 1
        elif motivation == 0:
            if result_negative.get(bed_cat, None):
                result_negative[bed_cat] += 1
            else:
                result_negative[bed_cat] = 1




def get_from_nectar():
    remove_dulp = localServer['result']
    map_fun = """function(doc){emit({twitter_id:doc.twitter_id, text:doc.text, coordinates:doc.coordinates},1)}"""
    design = {'views': {'get_twitters': {'map': map_fun}}}
    remove_dulp["_design/twitterlist1"] = design
    twitter_list = remove_dulp.view("twitterlist1/get_twitters")

    for r in twitter_list:
        analysis_and_count(r.key['coordinates']['coordinates'], r.key['text'])
    del remove_dulp['_design/twitterlist1']

def get_from_file():
    file = open("new_db.json")

    for line in file:
        line = line.strip()
        if line.startswith('['):
            continue
        if line.startswith('{') and line.endswith(","):
            str_list = list(line)
            str_list.pop()
            line = "".join(str_list)
            js = json.loads(line)
            #print "add one from a file"
            #print line
            analysis_and_count(js['coordinates'], js['text'])

def get_positive_index():
    bed_num = ""
    #print "result_positive is" + str(result)
    #print "result_negative is" + str(result_negative)
    for rp in result:
        for rn in result_negative:
            if rp == rn:
                #print "rp is " + str(result[rp])
                #print "rn is " + str(result_negative[rn])
                if rp == 0:
                    bed_num = '0'
                if rp == 1:
                    bed_num = '1-49'
                if rp == 2:
                    bed_num = '50-99'
                if rp == 3:
                    bed_num = '100-199'
                if rp == 4:
                    bed_num = '200-499'
                if rp == 5:
                    bed_num = '>500'
                final_result[rp] = {'beds': bed_num, 'count': round(math.log10(result[rp] / result_negative[rn] + 2), 4)}

def save_result():
    del hospital_result['hr1']
    hospital_result['hr1'] = {"fields": final_result}

def getdata():
    result.clear()
    result_negative.clear()
    final_result.clear()
    get_from_nectar()
    get_from_file()

def analysis_thread():
    count_time = 0
    while True:
        count_time += 1
        print("start analysing hospital bed category, count = "+str(count_time))
        getdata()
        get_positive_index()
        save_result()
        print("analyze hospital bed category successful, next time in 6 hours")
        time.sleep(3600 * 6)

from threading import Thread
t = Thread(target=analysis_thread())
t.start()

