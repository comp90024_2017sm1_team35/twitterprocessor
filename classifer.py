# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

import numpy as np
from bs4 import BeautifulSoup
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import re
from nltk.stem.wordnet import WordNetLemmatizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.externals import joblib
import time

class classifer(object):

    def __init__(self,nb_loc,vocab_loc):
        #loading the nb classifer
        self.clf = joblib.load(nb_loc)
        #loading the vocab for countvectorizer
        self.vocabulary = joblib.load(vocab_loc)
        self.vectorizer = CountVectorizer(analyzer="word", \
                                     tokenizer=None, \
                                     preprocessor=None, \
                                     stop_words=None, \
                                     ngram_range=(1, 2), \
                                     min_df=50, \
                                     max_features=400, \
                                     binary=True, \
                                     vocabulary=self.vocabulary
                                     )
        #stop word and lemma
        self.stops = set(stopwords.words("english"))
        self.lemmatizer = WordNetLemmatizer()

    #function for major funtion
    #return 0 if is negative return 1 if is positive
    def prediction(self,text):
        start = time.time()
        # remove html
        text = BeautifulSoup(text,"lxml").get_text()
        # remove non-english word
        text = re.sub("[^A-Za-z]", " ", text)
        # turing to lower case
        words = text.lower()
        # split to the token
        token = word_tokenize(words)
        # remove stop word
        word = [w for w in token if not w in self.stops]
        # lemlise the words
        for i in range(0, len(word)):
            word[i] = self.lemmatizer.lemmatize(word[i])
        #back to string
        final = " ".join(word)
        #get bow feature
        feature = self.vectorizer.transform([final])
        #give feature to classifer to make prediction
        return self.clf.predict(feature)





