# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

from __future__ import division
import couchdb
from classifer import classifer
import find_where
import json
import math
import time


remoteServer_string = "http://130.56.249.50:5984"
localServer_string = "http://127.0.0.1:5984/"

remoteServer = couchdb.Server(remoteServer_string)
localServer = couchdb.Server(localServer_string)
twitter_db = remoteServer['twitter_storage']
analysis = localServer['analysis_result']
suburbs_result = localServer['suburbs_result']
clf = classifer("nb.pkl","vocabulary.pkl")

result = dict()
resulr_array = []
final_result = dict()

result_negative = dict()
result_array_negative = []
final_result_negative = dict()

result_feature = dict()
feature_list = []
final_result_feature = dict()

def analysis_and_count(coordinates, text):
    suburb_id = find_where.contains_point(coordinates)
    if not suburb_id:
        print "didn't find any place in Melbourne"
    else:
        print "analysing text: "
        motivation = clf.prediction(text)
        if motivation == 1:
            # it is positive
            if result.get(suburb_id, None):
                result[suburb_id] += 1
            else:
                result[suburb_id] = 1
        elif motivation == 0:
            if result_negative.get(suburb_id, None):
                result_negative[suburb_id] += 1
            else:
                result_negative[suburb_id] = 1




def get_from_nectar():
    remove_dulp = localServer['result']
    map_fun = """function(doc){emit({twitter_id:doc.twitter_id, text:doc.text, coordinates:doc.coordinates},1)}"""
    design = {'views': {'get_twitters': {'map': map_fun}}}
    remove_dulp["_design/twitterlist"] = design
    twitter_list = remove_dulp.view("twitterlist/get_twitters")

    for r in twitter_list:
        analysis_and_count(r.key['coordinates']['coordinates'], r.key['text'])
    del remove_dulp['_design/twitterlist']

def get_from_file():
    file = open("new_db.json")

    for line in file:
        line = line.strip()
        if line.startswith('['):
            continue
        if line.startswith('{') and line.endswith(","):
            str_list = list(line)
            str_list.pop()
            line = "".join(str_list)
            js = json.loads(line)
            print "add one from a file"

            analysis_and_count(js['coordinates'], js['text'])

def save_final_result():
    total_count = 0
    maximum = 0
    minimum = 0
    first = 1

    total_count_negative = 0
    maximum_negative = 0
    minimum_negative = 0
    first_negative = 1

    feature_value_list = []


    for suburb_id in result:
        if first == 1:
            maximum = result[suburb_id]
            minimum = result[suburb_id]
        resulr_array.append({"id": suburb_id, "count": result[suburb_id]})
        total_count += result[suburb_id]
        if result[suburb_id] > maximum:
            maximum = result[suburb_id]
        if result[suburb_id] < minimum:
            minimum = result[suburb_id]
        first = 0

    for suburb_id in result_negative:
        if first_negative == 1:
            maximum_negative = result_negative[suburb_id]
            minimum_negative = result_negative[suburb_id]
        result_array_negative.append({"id": suburb_id, "count": result_negative[suburb_id]})
        total_count_negative += result_negative[suburb_id]
        if result_negative[suburb_id] > maximum_negative:
            maximum_negative = result_negative[suburb_id]
        if result_negative[suburb_id] < minimum_negative:
            minimum_negative = result_negative[suburb_id]
        first_negative = 0

    final_result['suburbs'] = resulr_array
    final_result['total_count'] = total_count
    final_result['maximum'] = maximum
    final_result['minimum'] = minimum

    final_result_negative['suburbs'] = result_array_negative
    final_result_negative['total_count'] = str(total_count_negative)
    final_result_negative['maximum'] = maximum_negative
    final_result_negative['minimum'] = minimum_negative

    for positive_dict in final_result['suburbs']:
        for negative_dict in final_result_negative['suburbs']:
            if positive_dict['id'] == negative_dict['id']:
                value = positive_dict['count']/negative_dict['count']
                feature_list.append({'id': positive_dict['id'], 'count': round(math.log10(value+2), 3)})
    result_feature['suburbs'] = feature_list

    max_value = max(item['count'] for item in feature_list)
    min_value = min(item['count'] for item in feature_list)
    result_feature['max_value'] = max_value
    result_feature['min_value'] = min_value

    interval = (max_value - min_value)/8
    intervals = []

    for i in range(0,8,1):
        cur_min = min_value + i * interval
        intervals.append(cur_min)
    for i in range(0,8,1):
        intervals[i] = round(intervals[i], 3)

    interval_pos = (maximum - minimum)/8
    intervals_pos = []
    for i in range(0,8,1):
        cur_min = minimum + i * interval_pos
        intervals_pos.append(cur_min)
    intervals_pos[0] = 0

    intervals_pos[0] = 0
    for i in range(0, 8, 1):
        intervals_pos[i] = round(intervals_pos[i])

    interval_neg = (maximum_negative - minimum_negative) / 8
    intervals_neg = []
    for i in range(0, 8, 1):
        cur_min = minimum + i * interval_neg
        intervals_neg.append(cur_min)
    intervals_neg[0] = 0
    for i in range(0, 8, 1):
        intervals_neg[i] = round(intervals_neg[i])

    del suburbs_result['sr1']
    suburbs_result['sr1'] = {"fields": final_result, "colors": intervals_pos}
    del suburbs_result['sr2']
    suburbs_result['sr2'] = {"fields": final_result_negative, "colors": intervals_neg}
    del suburbs_result['sr3']
    suburbs_result['sr3'] = {"fields": result_feature, "colors": intervals}

def getdata():
    result.clear()
    del resulr_array[:]
    final_result.clear()

    result_negative.clear()
    del result_array_negative[:]
    final_result_negative.clear()

    result_feature.clear()
    del feature_list[:]
    final_result_feature.clear()

    get_from_nectar()
    get_from_file()

def analysis_thread():
    count_time = 0;
    while True:
        count_time += 1
        print("start analysing, count = "+str(count_time))
        getdata()
        save_final_result()
        print("analyze successful, next time in 6 hours")
        time.sleep(3600 * 6)

from threading import Thread
t = Thread(target=analysis_thread())
t.start()
