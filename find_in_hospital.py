# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

import json
from shapely.geometry.polygon import Polygon
from shapely.geometry import Point

json_object = json.load(open("hospital.geojson.json"))

suburb_polygons = {}
for suburb_info in json_object['features']:
    if suburb_info['geometry']['type'] == 'MultiPolygon':
        multiple_polygon = suburb_info['geometry']['coordinates'][0]
    elif suburb_info['geometry']['type'] == 'Polygon':
        multiple_polygon = suburb_info['geometry']['coordinates']
    polygons = []
    for polygon in multiple_polygon:
        suburb_coordinates = []
        for coordinates in polygon:
            suburb_coordinates.append((coordinates[0], coordinates[1]))
        polygons.append(Polygon(suburb_coordinates))
    suburb_polygons[suburb_info['id']] = polygons


# (long, lat) like (144.1235, -38.3238)
def contains_point(coordinates):
    point = Point(coordinates)
    for id in suburb_polygons.keys():
        for polygon in suburb_polygons[id]:
            if polygon.contains(point) or polygon.touches(point):
                return id

    return False