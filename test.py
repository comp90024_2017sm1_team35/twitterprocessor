# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

import couchdb
import json
import time
remoteServer_string = "http://130.56.249.50:5984"
localServer_string = "http://127.0.0.1:5984/"

remoteServer = couchdb.Server(remoteServer_string)
localServer = couchdb.Server(localServer_string)
twitter_db = remoteServer['twitter_storage']
remove_dulp = localServer.create('result')
time_start = time.time()


def delete_all():
    #rows = remove_dulp.view('_all_docs', include_docs=True)
    del localServer['result']
    remove_dulp = localServer.create('result')



def remove_dulplication(db,count):
    #clear data first
    delete_all()
    if(count > 1):
        del db["_design/twitterlist"]

    map_fun = """function(doc){
                                    if(doc.coordinates){
                                        emit({twitter_id:doc.id_str, text:doc.text, coordinates:doc.coordinates},1)
                                    };
                               }"""
    reduce_fun = "function(keys,values){return sum(values)}"
    design = {'views':{'get_twitters': {'map': map_fun,'reduce':reduce_fun}}}
    db["_design/twitterlist"] = design
    twitter_list = db.view("twitterlist/get_twitters", group=True)

    for r in twitter_list:
        twitter_id = r.key['twitter_id']
        text = r.key['text']
        coordinates = r.key['coordinates']
        doc = {"twitter_id": twitter_id, "coordinates": coordinates, "text": text}
        remove_dulp.save(doc)

def remove_dulp_thread():
    count_time = 0;
    while True:
        count_time += 1
        print("start removing dulplication, count = "+str(count_time))
        remove_dulplication(twitter_db,count_time)
        print("remove dulplication successful, next time in 6 hours")
        time.sleep(3600 * 6)

from threading import Thread
t = Thread(target=remove_dulp_thread())
t.start()

#remove_dulplication(twitter_db)
#end_time = time.time()

#print ("remove dulplication successful, consume time "+str(end_time-time_start)+"seconds")