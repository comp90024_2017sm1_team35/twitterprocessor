# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

import json
import termcolor
import timeit

start_time = timeit.default_timer()

print('+++++++++++++++++Start+++++++++++++++++')

flag = 1

with open('new_db.json', 'a') as my_file:
    my_file.write('['+'\n')

with open('bigTwitter.json', encoding="utf8") as f:
    for line in f:

        if line.lstrip().startswith('{'):
            dirty_line = ''.join(line)
            clean_line = dirty_line[:-1]
            clean_line = clean_line.rstrip(',')
            item = json.loads(clean_line)

            if -38.2143 <= item["json"]["coordinates"]["coordinates"][1] <= -37.4143 and 144.563 <= \
                    item["json"]["coordinates"]["coordinates"][0] <= 145.363:
                my_dict = {'coordinates': item['json']['coordinates']['coordinates'],'id': item['json']['id_str'],
                           'text': item['json']['text']}

                if flag != 1:
                    with open('new_db.json', 'a') as my_file:
                            my_file.write(',' + '\n')

                with open('new_db.json', 'a') as my_file:
                    my_file.write(json.dumps(my_dict))

                flag += 1

with open('new_db.json', 'a') as my_file:
    my_file.write(']' + '\n')

print('+++++++++++++++++End+++++++++++++++++')
elapsed = timeit.default_timer() - start_time
print ('Execution time:')
print (elapsed)