# Team 35 Melbourne
# Zhe Xu -----------741434
# Qi Huang --------732802
# Botian Chen ----750249
# Han Yu -----------744981
# Kun He-----------721391

from __future__ import division
import couchdb
from classifer import classifer
import find_in_hospital
import json
import math
import time


remoteServer_string = "http://130.56.249.50:5984"
localServer_string = "http://127.0.0.1:5984/"

remoteServer = couchdb.Server(remoteServer_string)
localServer = couchdb.Server(localServer_string)
twitter_db = remoteServer['twitter_storage']
analysis = localServer['analysis_result']
hospital_result = localServer['suburbs_result']
#hospital_result = localServer['hospital_result']
clf = classifer("nb.pkl","vocabulary.pkl")

result = dict()
result_negative = dict()
final_result = dict()
save = []


def initiate():
    json_object = json.load(open("hospital.geojson.json"))
    for hospital_info in json_object['features']:
        result[hospital_info['properties']['id']] = 0
        result_negative[hospital_info['properties']['id']] = 1


def get_hospital_id_by_id(hospital_id):

    json_object = json.load(open("hospital.geojson.json"))
    for hospital_info in json_object['features']:
        if hospital_info['id'] == hospital_id:
            return hospital_info['properties']['id']
    return None


def analysis_and_count(coordinates, text):
    hospital_id = find_in_hospital.contains_point(coordinates)
    if not hospital_id:
        print "not in the place near a hospital"
    else:
        # get the hospital_id from the file by the hospital_id
        hospital_number = get_hospital_id_by_id(hospital_id)

        motivation = clf.prediction(text)

        if motivation == 1:
            # it is positive
            #print "haha"
            #print "this is "+ str(result.get(hospital_number))
            result[hospital_number] += 1
            #print str(hospital_number)+"add1 is"+str(result[hospital_number])

        elif motivation == 0:
            #print "xixxi"
            result_negative[hospital_number] += 1


def get_from_nectar():
    remove_dulp = localServer['result']
    map_fun = """function(doc){emit({twitter_id:doc.twitter_id, text:doc.text, coordinates:doc.coordinates},1)}"""
    design = {'views': {'get_twitters': {'map': map_fun}}}
    remove_dulp["_design/twitterlist3"] = design
    twitter_list = remove_dulp.view("twitterlist3/get_twitters")

    for r in twitter_list:
        analysis_and_count(r.key['coordinates']['coordinates'], r.key['text'])
    del remove_dulp['_design/twitterlist3']


def get_from_file():
    file = open("new_db.json")

    for line in file:
        line = line.strip()
        if line.startswith('['):
            continue
        if line.startswith('{') and line.endswith(","):
            str_list = list(line)
            str_list.pop()
            line = "".join(str_list)
            js = json.loads(line)
            print "add one from a file"
            #print line
            analysis_and_count(js['coordinates'], js['text'])


def get_positive_index():
    #print "result_positive is" + str(result)
    #print "result_negative is" + str(result_negative)
    for rp in result:
        for rn in result_negative:
            if rp == rn:
                #print "rp is " + str(result[rp])
                #print "rn is " + str(result_negative[rn])
                final_result[rp] = {'id': rp, 'count': round(math.log10(result[rp] / result_negative[rn] + 2), 3)}

    for r in final_result:
        save.append(final_result[r])


def save_result():
    del hospital_result['sr4']
    hospital_result['sr4'] = {"fields": {"suburbs": save, "maximum": 111}}


def getdata():

    final_result.clear()
    del save[:]
    initiate()
    get_from_nectar()
    get_from_file()


def analysis_thread():
    count_time = 0
    while True:
        count_time += 1
        print("start analysing hospital, count = "+str(count_time))
        getdata()
        get_positive_index()
        save_result()
        print("analyze hospital successful, next time in 6 hours")
        time.sleep(3600 * 6)

from threading import Thread
t = Thread(target=analysis_thread())
t.start()
